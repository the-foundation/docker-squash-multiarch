###export CI_REGISTRY=docker.io
###export REGISTRY_HOST=docker.io
###export REGISTRY_PROJECT=thefoundation
###export REGISTRY_USER=yourUserName
###export REGISTRY_PASSWORD=asdasdasdasdadsasdasdasdasdasd
###export CI_COMMIT_SHA=00000000

exit_msg() { echo "EXIT:"$1 ; exit $2 ; }

(which docker-squash 2>/dev/null |grep -q docker-squash ) || (which apk && ( apk add --no-cache py3-pip && pip3 install docker-squash ))
(which docker-squash 2>/dev/null |grep -q docker-squash ) || ( which apt && ( apt update ; apt install -y python3-pip ;pip3 install docker-squash ))

push_silencer() {
  grep -v  -e "Layer already exists"
}
pull_silencer() {
  grep -v  -e "Extrac" -e "p to date" -e "Verifying Checksum" -e "Waiting" -e "Pulling fs layer" -e "Download complet" -e "Downloaded newer image" -e "Already exist"
}

docker_squash_singlearch() {
  target=$1
  docker pull ${target} 2>&1 | pull_silencer
  tmptag=$1_cache
  squashres=$(docker-squash --tag $tmptag $1 2>&1 |grep -v -e "docker-squash version" -e "Checking if squash" -e "Squashing file" >&2 ) ;
  docker tag $tmptag $1
  docker push $1
  docker rmi $tmptag 2>&1|grep -v Untagged  | tr -d '\n'
  echo "$squashres"
  ( echo "$squashres"|grep "no squashing is required" -q ) && { squash_happened=no  ; exit 33 ; } ;
  ( echo "$squashres"|grep "New squashed image ID"    -q ) && { squash_happened=yes ; exit 0  ; } ;
  (( echo "$squashres"|grep "no squashing is required" -q ) || ( echo "$squashres"|grep "New squashed image ID" -q ) ) || (echo "FAIL:COULD NOT DETECT WHAT HAPPENED  squash_happened=$squash_happened ";echo ;echo "LOG:";echo "$squashres")
}

echo "BUILD:EMPTY"
(
  test -e /tmp/emptycontainer || mkdir /tmp/emptycontainer 2>&1
  touch /tmp/emptycontainer/.emptyfile 2>&1
  (echo "FROM scratch"
  echo "ADD .emptyfile /") >/dev/shm/.emptyDockerfile
  docker build -t emptycontainer -f /dev/shm/.emptyDockerfile /tmp/emptycontainer  2>& 1 | tr -d '\n' ) 2>&1  |sed 's/$/|/g' | tr -d '\n';echo 
#docker run --rm -it -d --name registry-squash -p 127.0.0.1:5001:5000 registry:2;

#target=thefoundation/upgraded-operating-systems:alpine
#target=thefoundation/upgraded-operating-systems:ubuntu-bionic;
#target=thefoundation/hocker:php8.0-dropbear-fpm;
target=$1
echo "$target"|grep -q ":"|| target=${target}":latest"

#cachetarget=${target/:*/:squashlayer}_${target/*:}

mlist="";
#docker pull $target
manifest=$(docker manifest inspect "${target}" )
digestlist=$(echo "$manifest" |jq -c '.manifests[]');
echo "${digestlist}"|wc -w |grep ^0$ && { echo  " NO DIGEST FOUND- NOT MULTIARCH IMAGE-TRYING singlearch" ;docker_squash_singlearch ${target} ; exit $? ; } ;
## count the digests
manicount=$(echo "$digestlist"|wc -l )
newmanifest="$manifest"
mlength=$(echo "$digestlist"|wc -l );
loop=0;
digestlist_raw=$(echo "$digestlist"|while read jsondigestline;do echo "$jsondigestline"|jq -c .digest|sed 's/"//g' ;done)

echo "PROCESSING:START for $manicount manifests"
#echo "$digestlist"|while read jsondigestline;do 

for digest in $digestlist_raw ;do
jsondigestline=$(echo "$digestlist"|grep ${digest})
#    digest=$(echo "$jsondigestline"|jq -c .digest|sed 's/"//g') ;
      arch=$(echo "$jsondigestline"|jq -c .platform.architecture|sed 's/"//g');
        os=$(echo "$jsondigestline"|jq -c .platform.os|sed 's/"//g');
    squash_happened=dontknow
    tmptag="127.0.0.1:5001/${target}_${arch}";
    cachetarget=${target/:*/:squashlayer}_${arch}_${target/*:}
    echo -n "RUNNING on ARCH $arch for ${target} DIGEST: $digest "
    #echo $tmptag;
    docker pull $target@$digest      2>&1 | pull_silencer  | tr -d '\n'   ;echo 
    echo -n "SQUASH:"
    squashres=$(docker-squash --tag $tmptag $target@$digest 2>&1 |grep -v -e "docker-squash version" -e "Checking if squash" -e "Squashing file" 2>&1)
    echo "$squashres"
    ( echo "$squashres"|grep -i "No such image" -q ) && squash_happened=no
    ( echo "$squashres"|grep "no squashing is required" -q ) && squash_happened=no
    ( echo "$squashres"|grep "New squashed image ID" -q    )  && squash_happened=yes
    (( echo "$squashres"|grep "no squashing is required" -q ) || ( echo "$squashres"|grep "New squashed image ID" -q ) ) || echo "FAIL:COULD NOT DETECT WHAT HAPPENED  squash_happened=$ squash_happened "
    [[ "$squash_happened" = "yes" ]]  && (echo -n "TAG_PUSH_RMI|"
    docker tag $tmptag $cachetarget   2>&1 ;
    docker push ${cachetarget}        2>&1 | push_silencer   |tr -d '\n';
    docker rmi  ${cachetarget}        2>&1 |grep -v Untagged | tr -d '\n';
    echo "PULL"
    docker pull $cachetarget          2>&1 | pull_silencer 
    ) |sed 's/^/...'$(echo ${target}_${arch}|tail -c 23 )' .|/g'
    #newdigest=$(docker image inspect $tmptag|jq '.[].RootFS.Layers[]'|cut -d'"' -f2)
    #newdigest=$(docker manifest inspect  $cachetarget |jq .config.digest|cut -d'"' -f2 )
    #newdigest=$(docker manifest inspect  $cachetarget |jq .layers[0].digest |cut -d'"' -f2)
    #newdigest=$(docker image inspect $cachetarget|jq '.[].RootFS.Layers[]'|cut -d'"' -f2)
    [[ "$squash_happened" = "yes" ]]  &&  newdigest=$(docker image inspect $cachetarget|jq -c '.[].RepoDigests[]'|cut -d'"' -f2)
    [[ "$squash_happened" = "yes" ]]  || newdigest="" 
    echo "DIGEST AFTER TAG_PUSH_RMI: $newdigest"
    [[ -z "${newdigest/ /}" ]] && echo "FAILING:DIGEST EMPTY"
    [[ -z "${newdigest/ /}" ]] && exit 1
    #newmanifest=$(echo "$newmanifest"|sed "s/$digest/$newdigest/g")
    #pulleddigest=$()
    #docker push $tmptag
    [[ "$squash_happened" = "yes" ]]  &&  (
    echo "ZEROING ${cachetarget} @registry | DELETING $tmptag @daemon"
    #docker rmi ${cachetarget}
    docker tag emptycontainer ${cachetarget} 2>&1|grep -v -e "http.StatusNotFound" -e "not found"; 
    docker push ${cachetarget} 2>&1 | push_silencer ;
    docker rmi $tmptag 
    #docker buildx imagetools create -t "${cachetarget}" ${cachetarget}@sha256:0000000000000000000000000000000000000000000000000000000000000000 2>&1 |sed 's/^/|/g'|tr -d '\n' &
    ) | tr -d '\n' 
    #echo -n " "$tmptag;
    mlist="$mlist $newdigest";
    echo 
    #echo "current digestlist: " $mlist
    #loop=$(($loop+1));
    # [[ "$loop" = "$manicount" ]] && ( echo;
    #    echo "listing done after $loop entries";
    #    # echo "$manifest" 
    #    echo "######"
    #    #echo "$newmanifest"|docker buildx imagetools create -f /dev/stdin -t thefoundation/buildcache:test3    
    #    #docker manifest create "$target" $mlist 
    #    # docker buildx imagetools create  -t "$target" $mlist 
    #    #realtarget=thefoundation/upgraded-operating-systems:squashlayer-out
    #    realtarget=$target
    #    [[ "$manicount" = $(echo $mlist|wc -w) ]] || ( echo  "NOT CREATING MANIFEST, INCOMING $manicount manifests do does not match: "$(echo $mlist|wc -w) )
    #    [[ "$manicount" = $(echo $mlist|wc -w) ]] && (
    #     echo creating manifest "$realtarget" $mlist ;
    #     docker buildx imagetools create  -t "$realtarget" $mlist 
    #    )
    #);
done
echo "NEW DIGEST LIST: $mlist"
[[ -z "$REAL_TARGET" ]] || realtarget=$REAL_TARGET
[[ -z "$REAL_TARGET" ]] && realtarget=$target
[[ "$manicount" = $(echo $mlist|wc -w) ]] || ( echo  "NOT CREATING MANIFEST, INCOMING $manicount manifests do does not match: "$(echo $mlist|wc -w) )
[[ "$manicount" = $(echo $mlist|wc -w) ]] && (
         echo creating manifest "$realtarget" $mlist ;
         docker buildx imagetools create  -t "$realtarget" $mlist 
        )
