# Docker Squash Multiarch

## sqashing multiarch docker images 
* squashing is done by tagging the images to cachetarget=${target/:*/:squashlayer}_${arch}_${target/*:}
* and afterwards force the registry to generate a new manifest
* finally the squashlayer tag is overwritten with an empty docker image

## example usage:
`bash docker-squash-multiarch.sh thefoundation/hocker:php8.0-dropbear-fpm `


<h3>A project of the foundation</h3>
<a href="https://the-foundation.gitlab.io/"><div><img src="https://hcxi2.2ix.ch/gitlab/the-foundation/docker-squash-multiarch/README.md/logo.jpg" width="480" height="270"/></div></a>


## Todo
- [ ] MORE DOCUMENTATION
- [ ] [Invite team members and collaborators](https://docs.gitlab.com/ee/user/project/members/)
- [ ] [Create a new merge request](https://docs.gitlab.com/ee/user/project/merge_requests/creating_merge_requests.html)
- [ ] [Automatically close issues from merge requests](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically)
- [ ] [Enable merge request approvals](https://docs.gitlab.com/ee/user/project/merge_requests/approvals/)
- [ ] [Automatically merge when pipeline succeeds](https://docs.gitlab.com/ee/user/project/merge_requests/merge_when_pipeline_succeeds.html)
